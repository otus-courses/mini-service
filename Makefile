#!make
ifneq (,$(wildcard ./.env))
    include ./.env
    export $(shell sed 's/=.*//' ./.env)
endif

PROJECT := mini-service
GROUP := otus-courses

ifndef APP_NAME
	export APP_NAME := $(PROJECT)
endif

VERSION := $(if $(TAG),$(TAG),$(if $(BRANCH_NAME),$(BRANCH_NAME),$(shell git describe --tags --exact-match 2>/dev/null || git symbolic-ref -q --short HEAD)))
BUILD_TIME := $(shell date -u '+%Y-%m-%d_%H:%M:%S')

export REGISTRY := registry.gitlab.com
export REGISTRY_PATH := otus-courses
export NOCACHE := $(if $(NOCACHE),"--no-cache")

export GO111MODULE=on

version:
	@echo "version: ${VERSION}"

## Dependencies
vendor:
	@go mod vendor

tidy:
	@go mod download
	@go mod tidy

deps: tidy vendor

gen: clean-gen gen-serv deps

gen-serv:
	@mkdir -p cmd
	@mkdir -p internal/generated
	@mkdir -p internal/app
	@swagger generate server \
		--with-context -f ./swagger/swagger.yml \
		-t ./internal/generated \
		-C ./swagger-gen/default-server.yml \
		--template-dir ./swagger-gen/templates \
		--main-package ${GROUP} \
		--name ${PROJECT}

clean-gen:
	@echo "Removing generated files..."
	@rm -rf ./internal/generated

## -------------------------
## Development
## -------------------------
run:
	@echo "Running app..."
	@go run -ldflags "-s -w -X gitlab.com/${GROUP}/${PROJECT}/internal/version.VERSION=${VERSION} -X gitlab.com/${GROUP}/${PROJECT}/internal/version.BUILD_TIME=${BUILD_TIME}" cmd/main.go

test:
	@echo "Running tests..."
	@go test ./internal/... -short -cover -count=1 -coverprofile cover.out

lint:
	@echo "Running golangci-lint..."
	@golangci-lint run

docker-build:
	@echo "Building docker image..."
	@docker build --ssh default --build-arg TAG=${VERSION} \
		--build-arg BUILD_TIME=${BUILD_TIME} \
		--build-arg APP_NAME=${PROJECT} \
		--build-arg GROUP=${GROUP} \
		--target builder \
		-t ${PROJECT}:build \
		-f Dockerfile .

build:
	@echo "Used envs: CI_PROJECT_NAME=${CI_PROJECT_NAME}"

ifeq ($(CI_PROJECT_NAME),${APP_NAME})
	@echo "Building docker image" [${REGISTRY}/${REGISTRY_PATH}/${APP_NAME}:${VERSION}]
	docker build ${NOCACHE} --pull --ssh default --progress=plain --build-arg TAG=${VERSION} \
		--build-arg BUILD_TIME=${BUILD_TIME} \
		--build-arg APP_NAME=${APP_NAME} \
		--build-arg GROUP=${GROUP} \
		-t ${REGISTRY}/${REGISTRY_PATH}/${APP_NAME}:${VERSION} \
		-f Dockerfile .
endif

push:
	@echo "Used envs: CI_PROJECT_NAME=${CI_PROJECT_NAME}"

ifeq ($(CI_PROJECT_NAME),${APP_NAME})
	@echo "Pushing docker image... [${REGISTRY}/${REGISTRY_PATH}/${PROJECT}:${VERSION}]"
	docker push ${REGISTRY}/${REGISTRY_PATH}/${APP_NAME}:${VERSION}
endif
