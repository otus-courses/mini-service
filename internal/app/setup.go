package app

import (
	"crypto/tls"
	"io"
	"log"
	"net/http"

	openapiErrors "github.com/go-openapi/errors"
)

type ServiceAPI interface {
	ServeError(rw http.ResponseWriter, r *http.Request, err error)
	LogCallback(str string, args ...interface{})
	ShutdownCallback(implShutdownCallback func())
	OnShutdown()
	SetupSwaggerHandlers(iapi interface{})
	ConfigureTLS(tlsConfig *tls.Config)
	ConfigureServer(scheme string, addr string)
}

type Service struct {
	closers []io.Closer
}

func New() *Service {
	return &Service{
		closers: make([]io.Closer, 0),
	}
}

func (srv *Service) AppendShutdownCloser(closer io.Closer) {
	srv.closers = append(srv.closers, closer)
}

func (srv *Service) OnShutdown() {
	for _, closer := range srv.closers {
		err := closer.Close()
		if err != nil {
			log.Printf("shutdown error closing %T: %v", closer, err)
		}
	}
}

func (srv *Service) LogCallback(str string, args ...interface{}) {
	log.Printf(str, args...)
}

func (srv *Service) ServeError(rw http.ResponseWriter, r *http.Request, err error) {
	openapiErrors.ServeError(rw, r, err)
}

func (srv *Service) ShutdownCallback(implShutdownCallback func()) {
	implShutdownCallback()
}

func (srv *Service) ConfigureTLS(_ *tls.Config) {
}

func (srv *Service) ConfigureServer(_ string, _ string) {
}
