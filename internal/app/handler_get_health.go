package app

import (
	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/otus-courses/mini-service/internal/generated/models"
	apiOperations "gitlab.com/otus-courses/mini-service/internal/generated/restapi/operations"
)

func (srv *Service) GetHealthHandler(_ apiOperations.GetHealthParams) middleware.Responder {
	return apiOperations.NewGetHealthOK().WithPayload(&models.Health{
		Status: Pt(models.HealthStatusOK),
	})
}
