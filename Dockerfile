FROM registry.gitlab.com/otus-images/base/golang-1.18.1-swagger-0.29.0:latest as builder

ARG TAG
ARG BUILD_TIME
ARG APP_NAME
ARG GROUP

ENV CGO_ENABLED 0
ENV APP_PATH gitlab.com/${GROUP}/${APP_NAME}

WORKDIR ${GOPATH}/src/${APP_PATH}

COPY . .

RUN make gen && \
    make deps && \
    go build -ldflags "-s -w -X ${APP_PATH}/internal/version.VERSION=${TAG} -X ${APP_PATH}/internal/version.BUILD_TIME=${BUILD_TIME}" \
        -installsuffix 'static' -o /bin/app ./cmd

FROM scratch as production

COPY --from=builder /bin/app /bin/default-server

ENTRYPOINT ["/bin/default-server"]
