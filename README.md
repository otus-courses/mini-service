## About

Simple service

## Getting Started

Need for starting:

1. Go 1.18
2. Goswagger 0.29 ([go-swagger](https://goswagger.io/install.html))

## Run local

first time run 

`make gen`

next 

`make run`

## Build docker image

just set tag