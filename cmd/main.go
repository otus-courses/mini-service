package main

import (
	"fmt"
	"log"
	"time"

	"github.com/go-openapi/loads"
	"github.com/vrischmann/envconfig"

	"gitlab.com/otus-courses/mini-service/internal/app"
	"gitlab.com/otus-courses/mini-service/internal/generated/restapi"
	"gitlab.com/otus-courses/mini-service/internal/generated/restapi/operations"
	"gitlab.com/otus-courses/mini-service/internal/version"
)

func main() {
	fmt.Printf("BuildTime - %s, Version - %s\n", version.BUILD_TIME, version.VERSION)

	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Panic(err)
	}

	api := operations.NewMiniServiceAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer server.Shutdown()

	srv := app.New()

	if err := server.ConfigureAPI(api, srv); err != nil {
		log.Panic(err)
	}

	var conf struct {
		HTTPBindPort    int
		GracefulTimeout time.Duration `envconfig:"default=10s"`
	}
	if err := envconfig.InitWithPrefix(&conf, "mini_service"); err != nil {
		log.Panic(err)
	}

	server.Port = conf.HTTPBindPort
	server.GracefulTimeout = conf.GracefulTimeout
	if err := server.Serve(); err != nil {
		log.Panic(err)
	}
}
